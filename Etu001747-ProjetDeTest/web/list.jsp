<%-- 
    Document   : list
    Created on : Nov 13, 2022, 11:28:17 AM
    Author     : Meylis
--%>

<%@page import="java.util.List"%>
<%@page import="model.Employer"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%!
    List<Employer> list = null;
%>
<% list = (List<Employer>) request.getAttribute("list");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>List</h3>


        <table border="1" width="100" cellpadding="1">
            <thead>
                <tr>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>Date de naissance</th>
                    <th>Email</th>
                </tr>
            </thead>
            <tbody>
                <%
                    for (int i = 0; i < list.size(); i++) {
                %> 
                <tr>
                    <td><%= ((Employer) list.get(i)).getNom()%> </td>
                    <td><%= ((Employer) list.get(i)).getPrenom()%></td>
                    <td><%= ((Employer) list.get(i)).getDatenaissance()%></td>
                    <td><%= ((Employer) list.get(i)).getEmail()%></td>
                </tr>
                <% }%>
            </tbody>
        </table>

    </body>
</html>
