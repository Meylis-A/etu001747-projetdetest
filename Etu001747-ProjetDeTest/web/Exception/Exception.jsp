<%-- 
    Document   : Exception
    Created on : Nov 16, 2022, 1:48:12 AM
    Author     : Meylis
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% Exception ex = (Exception) request.getAttribute("exception");%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Exception</title>
    </head>
    <body>
        <%= ex.printStackTrace() %>
    </body>
</html>
