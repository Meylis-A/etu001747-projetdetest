/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import controller.EmployerController;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.time.LocalDate;
import java.util.Calendar;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import utilitaire.ConnectionBase;

/**
 *
 * @author Meylis
 */
public class Employer {
    String id;
    String nom;
    String prenom;
    String email;
    Date datenaissance;

    public Date getDatenaissance() {
        return datenaissance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }    
    
    public void setDatenaissance(Date datenaissance) {
        this.datenaissance = datenaissance;
    }

    public Employer() {
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void ajout() throws Exception {
        PreparedStatement stmt = null;
        Connection con = null;
        ConnectionBase connection = new ConnectionBase();
        try {
            con = connection.getConnection();

            String sql = "insert into employe(id,nom,prenom,email,datenaissance) values (nextval('seq_employe'),?,?,?,?)";

            stmt = (PreparedStatement) con.prepareStatement(sql);
            stmt.setString(1, this.getNom());
            stmt.setString(2, this.getPrenom());
            stmt.setString(3, this.getEmail());
            stmt.setDate(4, this.getDatenaissance());

            System.out.println("sql : " + sql);

            stmt.executeUpdate();

        } catch (Exception ex) {
            throw ex;
        } finally {
            if (stmt != null) {
                stmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
    }

    public List<Employer> list() throws SQLException, Exception {
        Connection con = null;
        ConnectionBase connection = new ConnectionBase();

        Statement stmt = null;
        List<Employer> listEmp = new ArrayList<>();
        ResultSet res = null;

        try {
            con = connection.getConnection();
            String sql = "SELECT * FROM employe";

            stmt = con.createStatement();
            
            res = stmt.executeQuery(sql);
            
            while (res.next()) {
                Employer emp = new Employer();
                emp.setId(res.getString("id"));
                emp.setNom(res.getString("nom"));
                emp.setPrenom(res.getString("prenom"));
                emp.setEmail(res.getString("email"));
                emp.setDatenaissance(res.getDate("datenaissance"));

                listEmp.add(emp);
            }

        } catch (SQLException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        } finally {
            if (stmt != null) {
                stmt.close();
            }

            if (res != null) {
                res.close();
            }

            if (con != null) {
                con.close();
            }
        }
        return listEmp;
    }

}
