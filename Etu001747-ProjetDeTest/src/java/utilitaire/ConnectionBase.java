package utilitaire;



/**
 *
 * @author Meylis
 */
import java.sql.*;

public class ConnectionBase {

    public ConnectionBase() {
    }

    public Connection getConnection() throws Exception {
        Connection con = null;
        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection("jdbc:postgresql://localhost:5432/testframework", "tp", "meylis");
            //con.setAutoCommit(false);
            System.out.println("Connection Succes");
        } catch (ClassNotFoundException | SQLException e) {
            throw e;
        }
        return con;
    }
}
