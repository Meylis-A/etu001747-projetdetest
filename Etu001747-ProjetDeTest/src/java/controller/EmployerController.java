package controller;

import java.util.HashMap;
import java.util.List;
import model.Employer;
import modelview.ModelView;
import utilitaire.MyAnnotation;

/**
 *
 * @author Meylis
 */
public class EmployerController {

    Employer employer = new Employer();

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    @MyAnnotation.Urlmapping(url = "insertion", value = "ajout")
    public ModelView ajout() throws Exception {
        getEmployer().ajout();
        ModelView modelView = new ModelView();
        modelView.setPage("index.jsp");
        return modelView;
    }
    
    @MyAnnotation.Urlmapping(url = "liste", value = "list")
    public ModelView list() throws Exception{
        List<Employer> listEmp = getEmployer().list();
        HashMap hs = new HashMap();
        hs.put("list", listEmp);
        ModelView modelView = new ModelView();
        modelView.setData(hs);
        modelView.setPage("list.jsp");
        return  modelView;
    }

}
